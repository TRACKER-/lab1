#ifndef READ_H
#define READ_H

enum read_status
{
    READ_OK = 0,
    READ_ROW_FAILED,
    READ_PADDING_FAILED,
    READ_INVALID_HEADER,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS
};

#endif
