#include "rotate.h"

struct image rotate(const struct image img)
{
	uint64_t width = img.width;
	uint64_t height = img.height;
	struct image rotated_img = image_create(height, width);
	for (size_t row = 0; row < height; row++)
	{
		for (size_t column = 0; column < width; column++)
		{
			rotated_img.data[(column * rotated_img.width) + (rotated_img.width - row - 1)] = img.data[row * width + column];
		}
	}
	return rotated_img;
}
