#include "modules/bmp/bmp.h"
#include "modules/file/file.h"
#include "utils/rotate/rotate.h"

int main(int argc, char **argv)
{
    if (argc > 0 && argc < 3)
    {
        printf("Enter file names");
        return 1;
    }

    FILE *file_input = open_file(argv[1], "rb");
    FILE *file_output = open_file(argv[2], "wb");
    if (file_input == NULL)
    {
        if (file_output == NULL)
        {
            printf("Files don't exist\n");
        }
        else
        {
            close_file(file_output);
            printf("Input file doesn't exist\n");
        }
        return 1;
    }
    else
    {
        if (file_output == NULL)
        {
            close_file(file_input);
            printf("Output doesn't exist \n");
            return 1;
        }
    }

    struct image img = {0, 0, NULL};
    enum read_status read_result = from_bmp(file_input, &img);
    if (read_result != READ_OK)
    {
        printf("Reading is failed with code");
        return (int)(read_result);
    }

    struct image rotated_img = rotate(img);
    enum write_status write_result = to_bmp(file_output, &rotated_img);
    if (write_result != WRITE_OK)
    {
        printf("Writing is failed with code");
        return (int)(write_result);
    }
    image_free(img);
    image_free(rotated_img);

    close_file(file_input);
    close_file(file_output);

    return 0;
}
