#include "bmp.h"

static const uint32_t bfType = 0x4D42;
static const uint32_t biSize = 40;
static const size_t biBitCount = 24;

static size_t bmp_padding_by_width(uint64_t width)
{
    return 4 - (width * 3) % 4;
}

static void put_padding(struct image *img, FILE *out)
{
    uint8_t paddings[4];
    uint8_t new_padding = bmp_padding_by_width(img->width);
    for (size_t i = 0; i < new_padding; i++)
    {
        paddings[i] = 0;
    }
    fwrite(paddings, new_padding, 1, out);
}

static uint32_t get_image_bmp_size(struct image *img)
{
    return (sizeof(struct pixel) * img->height * img->width + bmp_padding_by_width(img->width)) * img->height;
}

static struct bmp_header create_bmp_header(struct image *img)
{
    struct bmp_header header;
    header.bfType = bfType;
    header.bfileSize = sizeof(struct bmp_header) + get_image_bmp_size(img);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = biSize;
    header.biHeight = img->height;
    header.biWidth = img->width;
    header.biPlanes = 1;
    header.biBitCount = biBitCount;
    header.biCompression = 0;
    header.biSizeImage = get_image_bmp_size(img);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum read_status from_bmp(FILE *in, struct image *img)
{
    struct bmp_header header = {0};
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
    {
        return READ_INVALID_HEADER;
    }
    if (header.bfType != bfType)
    {
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != 24 || !in)
    {
        return READ_INVALID_BITS;
    }

    *img = image_create(header.biWidth, header.biHeight);
    for (int i = 0; i < img->height; i++)
    {
        if (fread(img->data + i * img->width, image_size_row(img), 1, in) != 1)
        {
            return READ_ROW_FAILED;
        }
        if (fseek(in, bmp_padding_by_width(img->width), SEEK_CUR))
        {
            return READ_PADDING_FAILED;
        }
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image *img)
{
    struct bmp_header header = create_bmp_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) < 1)
    {
        return WRITE_ERROR;
    }

    fseek(out, header.bOffBits, SEEK_SET);

    if (img->data != NULL)
    {
        for (size_t i = 0; i < img->height; i++)
        {
            fwrite(img->data + i * img->width, image_size_row(img), 1, out);
            put_padding(img, out);
        }
    }
    return WRITE_OK;
}
