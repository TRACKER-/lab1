#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>
#include <stdlib.h>

struct pixel
{
    uint8_t b, g, r;
};

struct image
{
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(uint64_t width, uint64_t height);
void image_free(struct image img);
uint32_t image_size_row(struct image const *img);

#endif
