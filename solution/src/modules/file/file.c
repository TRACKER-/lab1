#include "file.h"

FILE *open_file(const char *file_path_name, const char *mode_name)
{
    FILE *file = fopen(file_path_name, mode_name);
    return file;
}

void close_file(FILE *file)
{
    fclose(file);
}
