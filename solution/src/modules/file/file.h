#ifndef FILE_H
#define FILE_H
#include <stdio.h>

FILE *open_file(const char *file_path_name, const char *mode_name);
void close_file(FILE *file);

#endif
